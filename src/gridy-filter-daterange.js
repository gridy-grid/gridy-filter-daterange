
import { TPL_PATH_AN } from "../../sk-core/src/sk-element.js";

import { FIELD_TITLE_AN, FILTER_MODE_AN, FILTER_INCLUSIVE_AN } from "../../gridy-grid/src/filter/gridy-filter.js";

import { GridyFilterDriver } from "../../gridy-grid/src/filter/gridy-filter-driver.js";

import { DateParse } from "../../dateutils/src/global.js";

import { GridyFilterDate } from "../../gridy-filter-date/src/gridy-filter-date.js";

import { FILTER_MODE_LESS, FILTER_MODE_INCL_LESS, FILTER_MODE_GREATER, FILTER_MODE_INCL_GREATER } 
	from "../../gridy-grid/src/datasource/data-source-local.js";
	


export class GridyFilterDaterange extends GridyFilterDate {
    
	get tplPath() {
        if (! this._tplPath) {
            this._tplPath = this.comp.confValOrDefault(TPL_PATH_AN, '/node_modules/gridy-filter-daterange/src/tpls/') + 'gridy-filter-daterange.tpl.html'; 
		}
        return this._tplPath;
    }
    
    get fromEl() {
        if (! this._fromEl) {
            this._fromEl = this.comp.querySelector('#rangeFrom');
        }
        return this._fromEl;
    }
    
    get toEl() {
        if (! this._toEl) {
            this._toEl = this.comp.querySelector('#rangeTo');
        }
        return this._toEl;
    }
    
    get fmt() {
		return this.fromEl.fmt;
	}
	
	get translations() {
		return {
			'EN': { 'from': 'From', 'to': 'To' },
			'RU': { 'from': 'От', 'to': 'До' },
			'DE': { 'from': 'Von', 'to': 'Zu' },
			'HI': { 'from': 'से', 'to': 'प्रति' },
			'CN': { 'from': '从', 'to': '到' }
		}
	}
	
	get curTranslations() {
		return this.translations[this.comp.locale.lang];
	}
        
    renderFilter() {
        this.comp.tplVars = Object.assign({ 
			'fieldTitle': this.comp.fieldTitleLabel, ...this.curTranslations }, this.comp.tplVars);

        this.render().then(() => {
            if (this.fromHandler) {
                this.fromEl.removeEventListener('skchange', this.fromHandler);
            }
            let fromHandler = function(event) {
                let filters = [];
                filters.push(
                    { value: this.fromEl.value, title: this.comp.getAttribute(FIELD_TITLE_AN), 
						mode: this.comp.hasAttribute(FILTER_INCLUSIVE_AN) ? FILTER_MODE_INCL_GREATER : FILTER_MODE_GREATER, 
						comparator: this.compareDates, ctx: this }
                );
                if (this.toEl.value) {
                    filters.push(
                        { value: this.toEl.value, title: this.comp.getAttribute(FIELD_TITLE_AN), 
							mode: this.comp.hasAttribute(FILTER_INCLUSIVE_AN) ? FILTER_MODE_INCL_LESS : FILTER_MODE_LESS, 
							comparator: this.compareDates, ctx: this }
                    );
                }
                this.comp.filters = filters;
                this.comp.changeFilter(event);
            }.bind(this);
            this.fromEl.addEventListener('skchange', fromHandler);
            if (this.toHandler) {
                this.toEl.removeEventListener('skchange', this.toHandler);
            }
            let toHandler = function(event) {
                let filters = [];
                if (this.fromEl.value) {
                    filters.push(
                        { value: this.fromEl.value, title: this.comp.getAttribute(FIELD_TITLE_AN), 
							mode: this.comp.hasAttribute(FILTER_INCLUSIVE_AN) ? FILTER_MODE_INCL_GREATER : FILTER_MODE_GREATER,  
							comparator: this.compareDates, ctx: this }
                    );
                }
                filters.push(
                    { value: this.toEl.value, title: this.comp.getAttribute(FIELD_TITLE_AN), 
						mode: this.comp.hasAttribute(FILTER_INCLUSIVE_AN) ? FILTER_MODE_INCL_LESS : FILTER_MODE_LESS, 
						comparator: this.compareDates, ctx: this }
                );
                this.comp.filters = filters;
                this.comp.changeFilter(event);
            }.bind(this);
            this.toEl.addEventListener('skchange', toHandler);
        });
    }
}
