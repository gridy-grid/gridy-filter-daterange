# GridyGrid filter daterage plugin


## Installation

```shell
npm i gridy-grid gridy-filter-daterange
```

markup as follows:


```html
<gridy-grid id="gridyGrid" base-path="/node_modules/gridy-grid/src" sort-field="$.title">

    <gridy-data-source
            fields='[{ "title": "Title", "path": "$.title" },{ "title": "Price", "path": "$.price"},{ "title": "Created", "path": "$.created"}]'></gridy-data-source>

    <gridy-filter dri="daterange" id="gridyFilter" field-title="Created"></gridy-filter>

    <gridy-table selectable="multi" selection-emit="ctrl+click" id="gridyTable">

    </gridy-table>

    <gridy-pager id="gridyPager2"></gridy-pager>
</gridy-grid>
<script type="module">
    let data = [];
    for (let i = 0; i < 25; i++) {
        let today = new Date();
        today.setDate(today.getDate() + i);
        data.push({title: 'row' + i, price: 100 * i, created: today.toLocaleDateString("en-US")})
    }
    let grid = document.querySelector('#gridyGrid');
    grid.addEventListener('bootstrap', () => {
        grid.dataSource.loadData(data);
        grid.table.whenRendered(() => {
            console.log('table rendered');
        });
    });
    customElements.define('gridy-grid', GridyGrid);
</script>
```

## Configuration Attributes

field-title - title of the field in datasource to be used for filtering

filter-inclusive - compare inclusive (including equal values)
